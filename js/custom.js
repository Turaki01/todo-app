let todo = [
    //     {
    //     text: 'Take my bath',
    //     completed: true
    // }, {
    //     text: 'Go to work',
    //     completed: true
    // }, {
    //     text: 'Eat lunch',
    //     completed: false
    // }, {
    //     text: 'Close from work',
    //     completed: false
    // }, {
    //     text: 'Go home',
    //     completed: true
    // }
]

const filters = {
    searchText: '',
    completed: false
}

const todoJSON = localStorage.getItem('todo')

if (todoJSON != null) {
    todo = JSON.parse(todoJSON)
}

const renderTodo = function(todo, filter) {
    const filterTodo = todo.filter(function(todo) {
        const searchText = todo.text.toLowerCase().includes(filters.searchText)
        const completeMatch = !filters.completed || !todo.completed
        return searchText && completeMatch
    })

    document.querySelector('#todoList').innerHTML = ''

    document.querySelector('#todos').innerHTML = ''

    // looping through filtered array
    filterTodo.forEach(function(todo, index) {
        // Todo display
        const todos = document.createElement('h6')
        todos.textContent = `${index + 1} ${todo.text}`
        document.querySelector('#todoList').appendChild(todos)
    })

    const incompletedTodo = filterTodo.filter(function(todo) {
        return !todo.completed
    })

    // Todo summary
    const summary = document.createElement('h4')
    summary.textContent = `you have ${incompletedTodo.length} todos left`
    document.querySelector('#todos').appendChild(summary)
}

renderTodo(todo, filters)

// searching through array
document.querySelector('#searchText').addEventListener('input', function(e) {
    // console.log(e.target.value)
    filters.searchText = e.target.value
    renderTodo(todo, filters)
})

// adding todo
document.querySelector('#addTodoForm').addEventListener('submit', function(e) {
    // console.log(e.target.addTodo.value)
    e.preventDefault()
    todo.push({
        text: e.target.addTodo.value,
        completed: false
    })

    localStorage.setItem('todo', JSON.stringify(todo))

    renderTodo(todo, filters)

    e.target.addTodo.value = ''
})

// listen on checkbox
document.querySelector('#hideCompleted').addEventListener('change', function(e) {
    filters.completed = e.target.checked
    renderTodo(todo, filters)
})